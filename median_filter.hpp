#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;

#define FILTER_SIZE 9
#define THRESHOLD_MEDIAN_FILTER 20

void median_filter(Mat src, Mat& dst);
