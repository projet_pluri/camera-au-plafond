#include <iostream>
#include <algorithm>
#include <stdio.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "FindPerson.hpp"

void NumberPerson(Mat src, int& numPer, vector<vector<Point> > &contour)
{
	findContours(src, contour, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	numPer = contour.size();
	cout<<"Number of detected person : " << contour.size() <<endl<<endl;
}
