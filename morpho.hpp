#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;

#define STRUCT_ELEM_SIZE 10
#define THRESHOLD_MORPHO 40

void Morpho_close(Mat src, Mat& dst);
