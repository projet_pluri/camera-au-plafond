#include <iostream>
#include <algorithm>
#include <stdio.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "FindPosition.hpp"

void FindPosition(Mat src, Mat& dst, vector<vector<Point> > &contour, int *posx, int *posy, int *dist)
{
	int min_x, max_x, min_y , max_y;
	Point pt1;
	Point pt2;
	Point center;	// Position of every person
	float distance, dist_x, dist_y;

	Mat rect = src;
	Point center_origine = Point(rect.cols/2,rect.rows/2);	// Center of image

	
	// Scan every edges
	for(unsigned int i=0; i < contour.size(); i++)
	{
		// Initialization
		min_x = contour[i][0].x;
		max_x = contour[i][0].x;
		min_y = contour[i][0].y;
		max_y = contour[i][0].y;

		// Scan every point in every edges and find the Point max and Point min
		for (unsigned int j = 0; j < contour[i].size(); j++)
		{
			if (min_x > contour[i][j].x)	min_x = contour[i][j].x;
			if (max_x < contour[i][j].x)	max_x = contour[i][j].x;
			if (min_y > contour[i][j].y)	min_x = contour[i][j].y;
			if (max_y < contour[i][j].y)	max_y = contour[i][j].y;
		}
		
		pt1 = Point(min_x, min_y);
		pt2 = Point(max_x, max_y);

		// Calcul position of person
		center = Point((min_x+max_x)/2, (min_y+max_y)/2);
		*(posx + i) = (min_x+max_x)/2;
		*(posy + i) = (min_y+max_y)/2;

		//Calcul distance of person to pyramid
		dist_x = abs(center_origine.x - center.x);
		dist_y = abs(center_origine.y - center.y);
		distance = (int)sqrt(pow((float)dist_x,2) + pow((float)dist_y,2));
		*(dist + i) = distance;

		cout << "Position of person "<< i+1<<"th = (" << center.x<<" , "<<center.y<<")" << endl;
		cout<< "Distance of person "<< i+1<<"th to the pyramid = "<<distance<<" pixels"<<endl;
		
		rectangle(rect,pt1,pt2,Scalar(255,255,255),-1,8,0);
		//line(rect, center, center_origine , Scalar(255,255,255), 1, 8, 0);
		//circle(rect, center, 1, Scalar(0,0,0), -1, 8, 0);
		cout << endl;
	}
	dst = rect;


	imwrite("rect.jpg",rect);
}
