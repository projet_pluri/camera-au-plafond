# pragma once

#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

void FindPosition(Mat src, Mat& dst, vector<vector<Point> > &contour, int *posx, int *posy, int *dist);
