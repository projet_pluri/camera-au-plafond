#include <iostream>
#include <algorithm>
#include <stdio.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "median_filter.hpp"

void median_filter(Mat src, Mat& dst)
{
	// Median Filtre
	medianBlur(src, dst, FILTER_SIZE);
	threshold(dst,dst,THRESHOLD_MEDIAN_FILTER,255,THRESH_BINARY);
}
