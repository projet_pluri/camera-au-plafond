#include <iostream>
#include <algorithm>
#include <stdio.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "morpho.hpp"

void Morpho_close(Mat src, Mat& dst)
{
	threshold(src, dst ,THRESHOLD_MORPHO, 255, THRESH_BINARY);

	// Create structure element
	Mat element = getStructuringElement( MORPH_RECT,
                                       Size( 2*STRUCT_ELEM_SIZE + 1, 2*STRUCT_ELEM_SIZE+1 ),
                                       Point( STRUCT_ELEM_SIZE, STRUCT_ELEM_SIZE ) );

	// Closing operation
	morphologyEx(dst, dst, MORPH_CLOSE, element);
}


