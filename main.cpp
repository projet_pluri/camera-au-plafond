#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <malloc.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "morpho.hpp"
#include "median_filter.hpp"
#include "Moyenneur_filtre.hpp"
#include "FindPerson.hpp"
#include "FindPosition.hpp"

using namespace cv;
using namespace std;

#define THRESHOLD 100
#define MAX_PERSON 10

typedef struct
{
	int xposition;
	int yposition;
	int distance;
}person;

typedef struct
{
	person persons[MAX_PERSON];
	int numberOfDetectedPersons;
}scene;

/** @function main */
int main( int argc, char** argv )
{
	// Declaration variables
	Mat src1,src2;
	Mat diff;
	Mat img_morpho;
	Mat img_median_filtre;
	Mat img_moyenneur_filtre;
	Mat rect;
	vector<vector<Point> > contour;
	int numPer = 0;
	int *posx = NULL;
	int *posy = NULL;
	int *dist = NULL;
	scene scenario;
	
	/////	LOAD IMAGES BACKGROUND AND PERSON
	char* image1 = argv[1];
	char* image2 = argv[2];
	src1 = imread( image1, CV_LOAD_IMAGE_GRAYSCALE);
	src2 = imread( image2, CV_LOAD_IMAGE_GRAYSCALE);

	if( argc < 2  || !src1.data || !src2.data )
	{
		printf( " No image data \n " );
		return -1;
	}
	
	// DIFFERENCE BETWEEN 2 BACKGROUND AND PERSON
	absdiff(src1,src2,diff);

	// MORPHOLOGY BY CLOSING
	//Morpho_close(diff, img_morpho);

	// FILTRE MEDIAN IMAGE
	//median_filter(diff, img_median_filtre);

	// FILTRE MOYENNEUR IMAGE
	moyenneur_filter(diff, img_moyenneur_filtre);

	// Load image to find edge
	Mat src, binary;
	//src = img_morpho;
	//src = img_median_filtre;
	src = img_moyenneur_filtre;

  	if (!src.data)
    	return -1;
	
	// THRESHOLD IMAGE
	threshold(src, binary, THRESHOLD, 255, CV_THRESH_BINARY);

	// FIND PERSON AND DISTANCE
	NumberPerson(binary, numPer, contour);
	scenario.numberOfDetectedPersons = numPer;
	
	// Allocation dynamique
	posx = (int *)malloc(numPer*sizeof(int));
	posy = (int *)malloc(numPer*sizeof(int));
	dist = (int *)malloc(numPer*sizeof(int));

	// Find position of person and distance to pyramid
	FindPosition(binary, rect, contour, posx, posy, dist);
	
	for(int i = 0; i < numPer; i++)
	{
		scenario.persons[i].xposition = posx[i];
		scenario.persons[i].yposition = posy[i];
		scenario.persons[i].distance = dist[i];
	}
	

	waitKey(0);
	return(0);
}




