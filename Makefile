CC=g++
CFLAGS=-W -Wall -Wextra -ansi -pedantic -O3
LDFLAGS=-lopencv_core -lopencv_highgui -lopencv_imgproc
EXEC=test_opencv
SRC= $(wildcard *.cpp)
OBJ= $(SRC:.cpp=.o)

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS) -L /usr/lib

%.o: %.cpp
	$(CC) -o $@ -c $< $(CFLAGS) -I /usr/include/opencv -L /usr/lib

.PHONY: clean mrproper

clean:
		rm -rf *.o

mrproper: clean
		rm -rf $(EXEC)



#g++ -O3 -g -Wall -Wextra main.cpp -o main -I /usr/include/opencv -L /usr/lib -lopencv_core -lopencv_highgui
