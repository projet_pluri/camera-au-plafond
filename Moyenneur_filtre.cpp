#include <iostream>
#include <algorithm>
#include <stdio.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Moyenneur_filtre.hpp"

void moyenneur_filter(Mat src, Mat& dst)
{
	// Moyenneur Filtre
	blur( src, dst, Size( KERNEL_SIZE, KERNEL_SIZE ), Point(-1,-1) );
	imwrite ("dst1.jpg",dst);
	threshold(dst,dst,THRESHOLD_MOYENNEUR_FILTER,255,THRESH_BINARY);
	imwrite ("dst2.jpg",dst);
}
