#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;

#define KERNEL_SIZE 3
#define THRESHOLD_MOYENNEUR_FILTER 15

void moyenneur_filter(Mat src, Mat& dst);
